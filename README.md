# Official sensitivity curves from O2

Files with .txt extension are two-column frequency, ASD ASCII files.

Files with .ecsv extension have ECSV comments lines in the file headers.

## L1 curve with no subtraction: 
`2017-08-06_DCS_C02_L1_O2_Sensitivity_strain_asd` 

From https://dcc.ligo.org/LIGO-G1801951

"These are calibrated strain and displacement spectra from a representative best
of O2, taken on Aug 06 2017.

This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C02," DCS frames
in LIGO jargon). We expect the uncertainty in calibration at this time 5% and 3
deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is larger and
unquantified outside this band). See P1600139 for more details on the estimated
uncertainty.

This data has no offline, auxiliary sensor subtraction performed on it.

For details on how the ASD was calculated, see T1500365."

***

## L1 curve with cleaning and subtraction: 
`2017-08-06_DCH_C02_L1_O2_Sensitivity_strain_asd`
 
From https://dcc.ligo.org/LIGO-G1801952

"These are calibrated strain and displacement spectra from a representative best
of O2, taken on Aug 6 2017.

This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C02," DCH frames
in LIGO jargon). We expect the uncertainty in calibration at this time 5% and 3
deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is larger and
unquantified outside this band). See P1600139 for more details on the estimated
uncertainty.

This data has had offline, auxiliary sensor subtraction performed on it. See P1800169 for details.

For details on how the ASD was calculated, see T1500365."

***

## H1 curve with no subtraction: 
`2017-06-10_DCS_C02_H1_O2_Sensitivity_strain_asd`


From https://dcc.ligo.org/LIGO-G1801949

"These are calibrated strain and displacement spectra from a representative best
of O2, taken on Jun 10 2017.

This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C02," DCS frames
in LIGO jargon). We expect the uncertainty in calibration at this time 5% and 3
deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is larger and
unquantified outside this band). See P1600139 for more details on the estimated
uncertainty.

This data has no offline, auxiliary sensor subtraction performed on it.

For details on how the ASD was calculated, see T1500365."

***

## H1 curve with cleaning and subtraction:
`2017-06-10_DCH_C02_H1_O2_Sensitivity_strain_asd`

From https://dcc.ligo.org/LIGO-G1801950

"These are calibrated strain and displacement spectra from a representative best
of O2, taken on Jun 10 2017.

This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C02," DCH frames
in LIGO jargon). We expect the uncertainty in calibration at this time 5% and 3
deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is larger and
unquantified outside this band). See P1600139 for more details on the estimated
uncertainty.

This data has had offline, auxiliary sensor subtraction performed on it. See P1800169 for details.

For details on how the ASD was calculated, see T1500365."

***
